//
//  Oscillator.m
//  Swift Synth
//
//  Created by Cezary Kopacz on 18/06/2020.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Oscillator.h"

@implementation Oscillator

static float _amplitude = 1.0f;
static float _frequency = 440.f;

+ (float)amplitude
{
    return _amplitude;
}

+ (void)setAmplitude:(float)amplitude
{
    _amplitude = amplitude;
}

+ (float)frequency
{
    return _frequency;
}

+ (void)setFrequency:(float)frequency
{
    _frequency = frequency;
}

// MARK: - Signals

+ (Signal)sine
{
    return [^(float time) {
        return Oscillator.amplitude * sinf(2.0 * M_PI * Oscillator.frequency * time);
        
    } copy];
}

+ (Signal)triangle
{
    return [^(float time) {
        double period = 1.0 / (double)Oscillator.frequency;
        double currentTime = fmod((double)time, period);
        double value = currentTime / period;
        double result = 0.0;
        
        if (value < 0.25) {
            result = value * 4;
        } else if (value < 0.75) {
            result = 2.0 - (value * 4.0);
        } else {
            result = value * 4.0 - 4.0;
        }
        
        return Oscillator.amplitude * (float)result;
    } copy];
}

+ (Signal)sawtooth
{
    return [^(float time) {
        double period = 1.0 / Oscillator.frequency;
        double currentTime = fmod((double)time, period);

        return Oscillator.amplitude * (float)(currentTime / period * 2 - 1.0f);
    } copy];
}

+ (Signal)square
{
    return [^(float time) {
        double period = 1.0 / Oscillator.frequency;
        double currentTime = fmod((double)time, period);
        
        return ((currentTime / period) < 0.5) ? Oscillator.amplitude : -1.0f * Oscillator.amplitude;
        
    } copy];
}

+ (Signal)whiteNoise
{
    return [^(float time) {
        float random = (float)(arc4random() % ((unsigned)RAND_MAX + 1)) / (float)((unsigned)RAND_MAX + 1);
        
        return Oscillator.amplitude * random;
    } copy];
}

@end
