//
//  Synth.m
//  Swift Synth
//
//  Created by Cezary Kopacz on 18/06/2020.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "Synth.h"

@interface Synth()

@property (copy, nonatomic) Signal signal;

@property (nonatomic) float time;
@property (nonatomic) double sampleRate;
@property (nonatomic) float deltaTime;

@property (nonatomic, strong) AVAudioEngine *audioEngine;
@property (nonatomic, strong) AVAudioSourceNode *sourceNode;

@end

@implementation Synth

@synthesize isPlaying;

- (AVAudioSourceNode *)sourceNode
{
    if (_sourceNode == nil) {
        _sourceNode = [[AVAudioSourceNode alloc] initWithRenderBlock:^OSStatus(BOOL *isSilence, const AudioTimeStamp *timestamp, AVAudioFrameCount frameCount, AudioBufferList *audioBufferList) {
            
            [self clearBufferList:audioBufferList];
            
            for(int frame = 0; frame < frameCount; frame++) {
                float sampleVal = self.signal(self.time);
                self.time += self.deltaTime;
                
                for(int idx = 0; idx < audioBufferList->mNumberBuffers; idx++) {
                    AudioBuffer buff = audioBufferList->mBuffers[idx];
                    
                    float *sample = (float *)buff.mData;
                    sample[frame] = sampleVal;
                }
            }
            
            return noErr;
        }];
    }
    
    return _sourceNode;
}

- (void)clearBufferList:(AudioBufferList*)bufferList
{
    for (int i = 0; i < bufferList->mNumberBuffers; i++) {
        memset(bufferList->mBuffers[i].mData, 0, bufferList->mBuffers[i].mDataByteSize);
    }
}

+ (instancetype)sharedInstance
{
    static Synth *instance;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[Synth alloc] init];
    });
    
    return instance;
}

+ (instancetype)shared
{
    return Synth.sharedInstance;
}

- (instancetype)init
{
    self = [self initWithSignal:Oscillator.sine];
    return self;
}

- (instancetype)initWithSignal:(Signal)signal
{
    self = [super init];
    
    if (self) {
        self.audioEngine = [[AVAudioEngine alloc] init];
        
        AVAudioMixerNode *mainMixer = self.audioEngine.mainMixerNode;
        AVAudioOutputNode *outputNode = self.audioEngine.outputNode;
        AVAudioFormat *format = [outputNode inputFormatForBus:0];
        
        self.time = 0.0f;
        
        self.sampleRate = format.sampleRate;
        self.deltaTime = 1.0f / (float)self.sampleRate;
        
        self.signal = [signal copy];
        
        AVAudioFormat *inputFormat = [[AVAudioFormat alloc] initWithCommonFormat:format.commonFormat sampleRate:self.sampleRate channels:1 interleaved:format.isInterleaved];
        
        [self.audioEngine attachNode:self.sourceNode];
        [self.audioEngine connect:self.sourceNode to:mainMixer format:inputFormat];
        [self.audioEngine connect:mainMixer to:outputNode format:nil];
        mainMixer.outputVolume = 0;

        NSError *error;
        [self.audioEngine startAndReturnError:&error];
        
        if (error != nil) {
            NSLog(@"Could not start audioEngine %@", error);
        } else {
            NSLog(@"audio engine started");
        }
    }
    
    return self;
}

- (void)setWaveformTo:(Signal)signal
{
    self.signal = signal;
}

- (float)volume
{
    return self.audioEngine.mainMixerNode.outputVolume;
}

- (void)setVolume:(float)volume
{
    self.audioEngine.mainMixerNode.outputVolume = volume;
    if(self.isPlaying) {
        self.isPlaying(volume != 0.0);
    }
}

@end
