//
//  Synth.h
//  Swift Synth
//
//  Created by Cezary Kopacz on 18/06/2020.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Oscillator.h"

@interface Synth : NSObject

@property (class, readonly) Synth *shared;
@property (nonatomic) float volume;

@property (nonatomic, copy) void (^isPlaying)(BOOL);

- (instancetype)initWithSignal:(Signal)signal;
- (void)setWaveformTo:(Signal)signal;

@end
