//
//  Oscillator.h
//  Swift Synth
//
//  Created by Cezary Kopacz on 18/06/2020.
//  Copyright © 2020 Grant Emerson. All rights reserved.
//


typedef float (^Signal) (float signal);

typedef NS_ENUM(NSInteger, Waveform) {
    sine, triangle, sawtooth, square, whiteNoise
};

@interface Oscillator : NSObject

@property (class) float amplitude;
@property (class) float frequency;

@property (class, copy, readonly) Signal sine;
@property (class, copy, readonly) Signal triangle;
@property (class, copy, readonly) Signal sawtooth;
@property (class, copy, readonly) Signal square;
@property (class, copy, readonly) Signal whiteNoise;


@end
